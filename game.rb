class Game

  def initialize
    @letters = ("a".."z").to_a
    @word = words.sample
    @attempts = 5
    @word_teaser = ""
    @word.first.size.times do
      @word_teaser += "_ "
    end
  end

  def words
    [
      ["Apple", "Juice"],
      ["Boy", "Samuel"],
      ["Cat", "Jaguar"]
    ]
  end

  def display_teaser(last_guess = nil)
    update_word_teaser(last_guess) unless last_guess.nil?
    puts "Myster word: #{@word_teaser} \n\n"
    if !@word_teaser.include?("_")
      puts "Well Done!"
      exit
    else
      guess_attempt
    end
  end

  def update_word_teaser(last_guess)
    current_teaser = @word_teaser.split
    current_teaser.each_with_index do |letter, index|
      if letter == '_' && (@word.first[index].upcase == last_guess || @word.first[index].downcase == last_guess)
        current_teaser[index] = last_guess
      end
    end
    @word_teaser = current_teaser.join(' ')
  end

  def good_progress(guess)
    display_teaser(guess)
  end

  def guess_attempt
    if @attempts > 0
      puts "Enter a letter: "
      @guess = gets.chomp
      correct = @word.first.upcase.include?(@guess) || @word.first.downcase.include?(@guess)
      if correct
        system "cls"
        puts "Good guess!"
        header
        good_progress(@guess)
      else
        system "cls"
        puts "Wrong guess, please try again"
        header
        display_teaser
        @attempts -= 1
          puts "You have #{@lives} remaining attempts"
          guess_attempt
      end
    else
      puts "Game Over"
    end
  end

  def header
    puts "* Your word is having a #{@word.first.size} character"
    puts "* Your clue is #{@word.last}"
  end

  def start
    header
    display_teaser
    guess_attempt
  end

end

game = Game.new
game.start